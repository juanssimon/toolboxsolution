var rp = require("request-promise");
if (!process.argv[2]) {
  console.log("The script expects one argument");
  console.log('e.g. node client.js "Request body"');
  process.exit(1);
}
var options = {
  method: "POST",
  uri: "http://localhost:8080/echo",
  body: process.argv[2],
  headers: {
    "content-type": "text/plain"
  }
};

rp(options)
  .then(function(parsedBody) {
    console.log("The API responded with: %s", parsedBody);
  })
  .catch(function(err) {
    if (err.error.errno === "ECONNREFUSED") {
      console.error(
        'Unable to connect to the API. Please make sure it is running.\nYou can run it with "npm start"'
      );
    } else {
      console.log("Unhandled error: %s\n%s", err.error.errno, err.message);
    }
  });
