const express = require("express");
var bodyParser = require("body-parser");

// App
const app = express();
app.use(bodyParser.text());
app.post("/echo", (req, res) => {
  res.send(req.body);
});

module.exports = app;
