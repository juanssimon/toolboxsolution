# Desafio toolbox

## SETUP:

```
git clone git@bitbucket.org:juanssimon/toolboxsolution.git
cd toolboxsolution
npm install
```

## Servidor:

```
npm start
```

## Testing:

```
npm test
```

## Cliente:

```
node client.js "request body"
```
