var app = require("../app");
var chai = require("chai");
var request = require("supertest");
var expect = chai.expect;

describe("Testing complete API", function() {
  var url = "http://localhost:3000";
  var server;
  before(function() {
    server = app.listen(3000);
  });

  after(function() {
    server.close();
  });
  describe("#POST", function() {
    it("/echo Should echo on short string", function(done) {
      request(url)
        .post("/echo")
        .set("Content-Type", "text/plain")
        .send("test 114214")
        .end(function(err, res) {
          expect(res.text).to.equal("test 114214");
          done();
        });
    });
    it("/echo Should echo on long string full of special chars", function(done) {
      request(url)
        .post("/echo")
        .set("Content-Type", "text/plain")
        .send("test +_)(*&^%$#@#$%^&*()_}J:}HAPR")
        .end(function(err, res) {
          expect(res.text).to.equal("test +_)(*&^%$#@#$%^&*()_}J:}HAPR");
          done();
        });
    });
    it("/echo Should echo on empty string", function(done) {
      request(url)
        .post("/echo")
        .set("Content-Type", "text/plain")
        .send("")
        .end(function(err, res) {
          expect(res.text).to.equal("");
          done();
        });
    });
    it("/echo Should echo empty on no body", function(done) {
      request(url)
        .post("/echo")
        .set("Content-Type", "text/plain")
        .end(function(err, res) {
          expect(res.text).to.equal("");
          done();
        });
    });
  });
  describe("#GET", function() {
    it("/echo return a 404 status", function(done) {
      request(url)
        .get("/echo")
        .set("Content-Type", "text/plain")
        .end(function(err, res) {
          expect(res.status).to.equal(404);
          done();
        });
    });
  });
});
